# alpine-nginx-with-dav

#### [alpine-x64-nginx-with-dav](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nginx-with-dav/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-nginx-with-dav "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-nginx-with-dav "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nginx-with-dav/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nginx-with-dav/)
#### [alpine-aarch64-nginx-with-dav](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-nginx-with-dav/)
#### [alpine-armhf-nginx-with-dav](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-nginx-with-dav/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [nginx-with-dav](http://nginx-with-dav.org/)
    - nginx-with-dav [engine x] is an HTTP and reverse proxy server, a mail proxy server, and a generic TCP/UDP proxy server, originally written by Igor Sysoev.
    - WebDAV support.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 80:80/tcp \
           -p 443:443/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpinex64build/alpine-x64-nginx-with-dav:latest
```

* aarch64
```sh
docker run -d \
           -p 80:80/tcp \
           -p 443:443/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpineaarch64build/alpine-aarch64-nginx-with-dav:latest
```

* armhf
```sh
docker run -d \
           -p 80:80/tcp \
           -p 443:443/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpinearmhfbuild/alpine-armhf-nginx-with-dav:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:80/](http://localhost:80/)
    - If you want to use custom setting, you need to create `nginx.conf` and add `-v nginx.conf:/conf.d/nginx/nginx.conf` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |
| 443/tcp            | HTTPS port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-nginx-with-dav](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-nginx-with-dav/)
* [forumi0721alpineaarch64build/alpine-aarch64-nginx-with-dav](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-nginx-with-dav/)
* [forumi0721alpinearmhfbuild/alpine-armhf-nginx-with-dav](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-nginx-with-dav/)

